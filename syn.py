#python 3.6
#
#Author:	Vit Solcik
#Date:		30.3.2017
#File:		syn.py

import sys
import getopt
import re
import pprint
from collections import namedtuple
from pprint import pprint

fileInput = sys.stdin
fileOutput = sys.stdout
fileFormat = None
brFlag = False

class tableClass:
	regex = None
	formats = None

##
## @brief      Main function
##
def main():
	arguments()

	if fileFormat != None:
		table = format()
		output = apply(table)
	else:
		output = fileInput.read()

	if brFlag == True:
		output = output.replace("\n", "<br />\n")

	fileOutput.write(output)


def regexConvert(regex):
	neg = ''
	pytReg = ''
	flag = False

	for i in range(0, len(regex)):
		
		if(regex[i] == '%' or flag):


			if(flag == False):
				flag = True
				continue

			if(regex[i] == 's'):
				pytReg += '[' + neg + ' \t\n\r\f\v]'

			elif(regex[i] == 'a'):
				if(neg == '^'):
					pytReg += '[[^A-Z]|[A-Z]]'
				else:
					pytReg += '^.'

			elif(regex[i] == 'l'):
				pytReg += '[' + neg + 'a-z]'

			elif(regex[i] == 'd'):
				pytReg += '[' + neg + '0-9]'

			elif(regex[i] == 'L'):
				pytReg += '[' + neg + 'A-Z]'

			elif(regex[i] == 'W'):
				pytReg += '[' + neg + '0-9a-zA-Z]'

			elif(regex[i] == 'w'):
				pytReg += '[' + neg + 'a-zA-Z]'

			elif(regex[i] == 'n'):
				pytReg += '[' + neg + '\n]'

			elif(regex[i] == 't'):
				pytReg += '[' + neg + '\t]'

			elif(regex[i] in '.|!*+()%'):
				pytReg += neg + '\\' + regex[i]

			else:
				print("Spatny regularni vyraz",file=sys.stderr)
				sys.exit(4)

			neg = ''
			flag = False

		elif(regex[i] == '!'):
			
			if(neg == '^'):
				print("Spatny regularni vyraz",file=sys.stderr)
				sys.exit(4)
			neg = '^'

		elif(regex[i] == '.'):
			
			if(regex[i-1] == '.' or i == 0 or i == len(regex)-1):
				print("Spatny regularni vyraz",file=sys.stderr)
				sys.exit(4)
			continue

		else:

			if neg == '^':
				pytReg += '[^'+ regex[i] +']'
				neg = ''

			if regex[i] in '\\^[]{}$?':
				pytReg += '\\'


			else:

				if(regex[i] == ')' and regex[i-1] == '('):
					print("Spatny regularni vyraz",file=sys.stderr)
					sys.exit(4)

				if((regex[i] == '|' and 
					(regex[i] == '|' and i == 0) or 
					regex[i-1] == '|') or 
					(regex[i] == '|' and i == len(regex)-1)):
					print("Spatny regularni vyraz",file=sys.stderr)
					sys.exit(4)

				pytReg += regex[i]

	return pytReg


##
## @brief      Gets the tag.
##
## @param      formatLine  The format line
## @param      flag        The flag
##
## @return     The tag.
##
def getTag(formatLine, flag):

	finalOut = ''
	
	for format in formatLine:
		out = '<'

		if(flag == False):
			out += '/'

		if(format == 'bold'):
			out += 'b'

		elif(format == 'italic'):
			out += 'i'

		elif(format == 'underline'):
			out += 'u'

		elif(format == 'teletype'):
			out += 'tt'

		elif(re.match('size:[1-7]', format)):
			out += 'font'
			if(flag):
				out += ' size=' + format[5:]
		
		elif(re.match('color:[0-9a-fA-F]{6}', format)):
			out += 'font'
			
			if(flag):
				out += ' color=#' + format[6:]
		
		else:
			print("Spatny format", file=sys.stderr)
			sys.exit(4)

		out += '>'

		if(flag):
			finalOut += out
		else:
			finalOut = out + finalOut

	return finalOut


##
## @brief      Apply all stuff
##
## @param      formatTable  The format table
##
## @return     { description_of_the_return_value }
##
def apply(formatTable):
	global fileInput
	inputContent = fileInput.read()
	
	array = [''] * (len(inputContent) + 1)

	i = 0
	for formatLine in formatTable.regex:
	

		regex = str(formatLine)
		regex = regexConvert(regex)

		formatList = formatTable.formats[i]

		outFlag = False
		found = False
		last = 0

		for match in re.finditer(regex, inputContent):
			found = True
			if (match.start()) != last and last != 0:
				array[last] = getTag(formatList, False) + array[last]
				array[match.start()] = array[match.start()] + getTag(formatList, True)

			if((match.end() != match.start()) and (outFlag == False)):
				array[match.start()] = array[match.start()] + getTag(formatList, True)

				outFlag = True
			else:
				array[match.start()] = array[match.start()]
				array[match.end()] = array[match.end()]

			last = match.end()

		if found == True:
			array[last] = getTag(formatList, False) + array[last]

		i += 1

	out = ''
	for i in range(len(inputContent)):
		out += array[i] + inputContent[i]
	out += array[len(inputContent)]

	#print(out)
	return out


## @brief      Separate regular exp. and format
##
## @return     The format table.
##
def format():
	global fileFormat
	formatTable = tableClass()
	formatTable.regex = []
	formatTable.formats = []


	formatContent = fileFormat.read();

	for line in formatContent.splitlines():
	
		if line != "":
			
			regex = re.findall(r'(.*)\t+', line)			
			regexStr = str(regex[0])

			
			# formats = line.strip(regexStr)
			lineStr = ''.join(line)

			formats = lineStr.split('\t', 1)
			formats = formats[1]

			formats = formats.split(',')
			formats = ''.join(formats)
			formats = formats.split()

			formatTable.regex.append(regexStr)
			formatTable.formats.append(formats)

	return formatTable	
	
##
## @brief      Check for arguments and open files
##
def arguments():
	global fileInput
	global fileOutput
	global fileFormat
	global brFlag



	opts, args = getopt.getopt(sys.argv[1:], "", ["help", "format=", "input=", "output=", "br"])
	

	argFormat = None
	argInput = None
	argOutput = None
	argBr = False

	for o, a in opts:
		if o in ("--help"):
			print_help()
			sys.exit()
		elif o in ("--format"):
			argFormat = a
		elif o in ("--input"):
			argInput = a
		elif o in ("--output"):
			argOutput = a
		elif o in ("--br"):
			brFlag = True
		else:
			assert False, "Bad args"

	if argInput == None:
		print("Bad args")
		sys.exit(1)

	# Open input file #
	try:
		fileInput = open(argInput, encoding='utf-8')
	except:
		print ("Chyba pri otevirani vstupniho souboru!\n",file=sys.stderr)
		sys.exit(2)

	# Open output file #
	if argOutput == None:
		fileOutput = open(1, 'w', encoding='utf-8', closefd=False)
	else:
		try:
			fileOutput = open(argOutput, 'w+', encoding='utf-8')
		except:
			print ("Chyba pri otevirani vystupniho souboru!\n",file=sys.stderr)
			sys.exit(3)

	# Open format file #
	if argFormat != None:
		try:
			fileFormat = open(argFormat, 'r', encoding='utf-8')
		except:
			print ("Spatny format formatovaciho souboru!\n",file=sys.stderr)
			sys.exit(4)



##
## @brief      Prints help message
##
def print_help():
	print("""
• --help 		vypise napovedu

• --format=filename 	určení formátovacího souboru. Soubor bude obsahovat libovolné
			množství formátovacích záznamů. Formátovací záznam se bude 
			skládat z regulárního výrazuvymezujícího formátovaný 
			text a příkazů pro formátování tohoto textu.

• --input=filename	určení vstupního souboru v kódování UTF-8.

• --output=filename 	určení výstupního souboru opět v kódování UTF-8 s naformátovaným
			vstupním textem. Formátování bude na výstupu realizováno 
			některými HTML elementy na popis formátování. 
			Regulární výrazy ve formátovacím souboru určí, který text se naformá-
			tuje, a formátovací informace přiřazené k danému výrazu určí,
			jakým způsobem se tento text naformátuje.

• --br přidá element 	<br /> na konec každého řádku původního vstupního textu (až po aplikaci
			formátovacích příkazů).

		""")

if __name__ == "__main__":
    main()